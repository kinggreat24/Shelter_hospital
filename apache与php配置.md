## Ubuntu Apache2与php配置

### Apache2安装与配置
1. Apache安装  
Apache 安装的是apache2,采用binary安装，bin文件位于/usr/local/bin  
`sudo apt install apache2`  
2. Apache配置  
Apache2配置文件为/etc/apache2/apache2.conf，关于端口的配置为/etc/apache2/ports.conf，关于module的配置在/etc/apache2/mods-enable以及/etc/apache2/mods-avialable,虚拟主机站点的配置在/etc/apache2/sites-available和/etc/apache2/sites-enabled目录下  
我们需要设置apache对应的WEB根目录：  
`vim /etc/apache2/apache2.conf`  
添加以下配置：  
    ```
    <Directory /home/bingo/apache>
            Options Indexes FollowSymLinks
            AllowOverride None
            Require all granted
    </Directory>
    ```  
    其中`/home/bingo/apache`是你选定要设置的apache的对应的WEB根目录  

    `cd /etc/apache2/sites-enabled`  
    `vim 000-default.conf`  
    修改`DocumentRoot`的值为`/home/bingo/apache`,也就是之前你配置的目录，修改`ServerName`为`127.0.1.1`  

    在/etc/apache2/sites-available目录下为配置的指定的虚拟主机的配置，其中000-default.conf为虚拟主机的默认配置：  
    其中DocumentRoot字段设置apache的根目录，设置`/home/bingo/apache`为当前根目录：
    `DocumentRoot /home/bingo/apache`  
    其中ErrorLog设置了apache的错误的日志，CustomLog设置了用户访问apache的记录日志；  
    其中/etc/apache2/sites-enabled目录下保存了当前使能的虚拟主机配置，为指向sites-available目录下的虚拟主机配置的链接.  
3. Apache的一些常规命令  
**关闭服务**  
`sudo service apache2 stop`  
`sudo apachectl stop`  
**开启服务**
`sudo service apache2 start`  
`sudo apachectl start`  
**重启服务**  
`sudo service apache2 restart`  
`sudo apachectl restart`  
Apache2是开机启动，默认的端口是80端口，所以访问localhost即可以访问Apache2的默认站点，Apache2的默认WebRoot目录在`/home/bingo/apache`下。

### Apache2下php的配置  
1. 安装当前版本下最新的php  
`sudo apt install php`  
2. 配置apache2与php命令  
`sudo apt-get install libapache2-mod-php`  
安装以上的模块之后，就可以在`/etc/apache2/mods-available`目录下找到`php7.0.load`以及`php7.0.conf`模块。  
3. 配置php.ini  
使用apache框架进行php上传文件会有文件大小以及时间等限制，需要修改php.ini文件  
打开php.ini文件  
`sudo vim /etc/php/7.0/apache2/php.ini`  
找到`max_execution_time`值，将默认的30改为0，及对php文件的操作时间没有限制  
找到`post_max_size`值，将默认的8M改成2048M，也就是允许post的最大文件大小为2G  
找到`upload_max_filesize`值，将默认的2M改为2000M，也就是允许最大的上传文件是2000M，最好让这个值略小于`post_max_size`  
修改完以上的值后，重启apache  
`sudo apachectl restart`  