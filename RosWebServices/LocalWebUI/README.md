## 说明
以下文档默认bingo-host已经配置好apache-php，主要讲述送餐服务如何与后端服务对接，不涉及流程的说明

### 网络配置服务说明
1. <span id=11><font color=blue><b>连接机器人bingo#2热点</b></font></span>  
手机WIFI连接**RUBAN**系统的内部路由器的无线网络  

    | 属性 | 值  | 
    |:----:|:-----:|
    | 热点名 | bingo |
    | 密码 | bingorobot |

2. <span id=12><font color=blue><b>编译rosbridge_patrol</b></font></span>  
将`LocalWebBackEnd\src\rosbridge_patrol`软件包拷贝到你的`catkin_ws`目录下，执行以下命令进行编译：  
`cd your_catkin_ws`  
`catkin_make`  

3. <span id=13><font color=blue><b>启动服务后台程序</b></font></span>  
`roslaunch rosbridge_patrol patrol_rosbridge_websocket.launch`  

4. <span id=14><font color=blue><b>浏览器访问服务地址</b></font></span>  
在手机浏览器的地址栏中输入`bingo-host`的地址，我们的**RUBAN**系统使用的IP是`192.168.2.3`，你讲看到以下的界面：
    <div align=center><img src="img/patrol_demo.png"/></div>  

    以下表格将介绍一下按钮点击之后的触发逻辑：  

    | 按钮名 | 执行操作  |
    |:----:|:-----:|
    | 开始录制数据包 | rostopic pub /launch_rosbag_record std_msgs/String "data: 'success'" |
    | 添加送餐点 | rostopic pub /add_patrol_point std_msgs/Empty "{}" |
    | 结束录制数据包 | rostopic pub /close_rosbag_record std_msgs/Empty "{}"<br/>rostopic pub /save_patrol_point std_msgs/String "data: 'success.json'" |
    | 下载数据包 | 下载数据包会将apache WEB根目录下的download/patrol_raw_data.tar.gz进行下载 |
    | 点击上传 | 会出现选择文件的browser选框，找到收到的压缩包文件，压缩包统一命名为patrol_map_config.tar.gz |
    | 确认提交 | 会将给定的压缩文件上传到apache WEB根目录下的upload目录下 |
    | 启动导航 | rostopic pub /loadPoints std_msgs/String "data: 'patrol_points.json'" <br/> rostopic pub /launch_amcl std_msgs/String "data: 'patrol_map_yaml.yaml'" |
    | 开始送餐 | rostopic pub /single_patrol std_msgs/Empty "{}"|
    | 结束送餐 | rostopic pub /isEnablePatrol std_msgs/Bool "data: false"|
    | 结束导航 | rostopic pub /close_amcl std_msgs/Empty "{}"|


    <font color=blue><b>
    值得注意的点：  
    1：结束录制数据包  
    结束按钮时将触发保存所有巡逻点  
    2：下载数据包  
    手机浏览器默认会下载并显示下载进度，最后保存在手机浏览器默认下载目录下，请注意这个压缩包的名字和位置是固定的，因此需要在结束录制之后，将rosbag包以及巡逻点的json文件自动打包成tar.gz的文件并放在指定目录下  
    3：点击上传  
    点击上传只选择了文件的路径，这个压缩文件的名字是固定的，并且里面将地图和相应的配置文件压缩在该包中  
    4：确认提交  
    确认提交会将压缩包提交到给定目录，后台程序需要将该压缩包解压并拷贝到软件包的指定位置  
    5: 启动导航  
    启动导航会出发两个消息发送，请注意下这里默认发出的文件名是否和软件包中的文件名能够对应上  
    </b></font>