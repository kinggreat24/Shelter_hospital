<html>
	<head>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	  <title>服务机器人服务配置</title>
	  <script type="text/javascript">
	  	//WebSocket = null;
	  </script>
	  <link href="/css/bootstrap.min.css" rel="stylesheet">
	  <link href="/css/style.css" rel="stylesheet">
	  <link href="/css/aupload.css" rel="stylesheet">
	  <link href="/css/upload_progress.css" rel="stylesheet">
	  <!-- Include these three JS files: -->
	  <script type="text/javascript" src="/js/swfobject.js"></script>
	  <script type="text/javascript" src="/js/web_socket.js"></script>
	  <script type="text/javascript" src="/js/jquery-1.12.1.min.js"></script>

	  <script type="text/javascript">
			if (typeof console == "undefined") {    this.console = { log: function (msg) {  } };}
			WEB_SOCKET_SWF_LOCATION = "/swf/WebSocketMain.swf";
			WEB_SOCKET_DEBUG = true;
			var ws;
			<!-- connect to the server -->
			var current_room_id = 0;

			var patients_info = [];
			
			function connect() {
				// create websocket
				ws = new WebSocket("ws://"+document.domain+":9090");
				//  when socket disconnect, call service
				ws.onopen = onopen;
				ws.onmessage = onmessage;
				ws.onclose = function() {
					console.log("connection clsed, reconnect");
					connect();
				};
				ws.onerror = function() {
					console.log("conect error");
				};
			}
			function onopen()
			{
				// shakehands
				var shakehands_data = '{"op":"shakehands","topic":"/None"}';
				console.log("websocket success:" + shakehands_data);
				ws.send(shakehands_data);
				// send the load patients message

				/*var msg ='{"op":"publish","topic":"/load_patients"}';
				console.log(msg);
				ws.send(msg);*/
			}
			function onmessage(e)
			{
				console.log(e.data);
				var data = eval("("+e.data+")");
				// parse the on message
				switch(data['op']){
		            case 'shakehands_ack':
		                console.log('shakehands ack received');
		                break;
					case 'patients_ack':
						console.log('patients ack received');
						patients_info = eval("("+data['data']+")");
						fresh_patients_checkboxes(48, patients_info);
						break;
					case 'load_points_ack':
						console.log('load_points_ack received');
						// send the load patients message
						var msg ='{"op":"publish","topic":"/load_patients"}';
						console.log(msg);
						ws.send(msg);
						break;
		            default:break;
		        }
			}

			function fresh_patients_checkboxes(checkboxes_num, patients_info)
			{
				for(var i = 0; i < checkboxes_num; i++){
					var room_id = patients_info[i].room_num;
					var patient_name = patients_info[i].name;
					var current_label_id = "label_patients_" + (i+1);
					var current_label_txt = room_id + '\n' + patient_name;
					document.getElementById(current_label_id).innerText = current_label_txt;
				}
			}

			function onclick_rosbag_record_start()
			{
				var msg ='{"op":"publish","topic":"/launch_rosbag_record"}';
				console.log(msg);
				ws.send(msg);
			}

			function onclick_rosbag_record_end()
			{
				var msg ='{"op":"publish","topic":"/close_rosbag_record"}';
				console.log(msg);
				ws.send(msg);
			}

			function onclick_add_patrol_point()
			{
				var room_id = prompt("请输入当前点房间号", current_room_id);
				if(room_id != null && room_id != ""){
					var room_id_num = parseInt(room_id);
					// console.log(room_id_num);
					if(room_id_num){
						current_room_id = room_id_num;
					}else{
						alert("房间号只能是数字\n请重新点击添加配送点");
						return;
					}
				}else{
					// if(confirm("房间号输入异常，是否重新输入")){
					// 	onclick_add_patrol_point();
					// }else{
					// 	return;
					// }
					alert("房间号输入异常\n请重新点击添加配送点");
					return;
				}
				var row = {};
				row.op = "publish";
				row.topic = "/add_patrol_point";
				row.data = current_room_id.toString();
				var msg = JSON.stringify(row);
				// var msg ='{"op":"publish","topic":"/add_patrol_point"}';
				console.log(msg);
				ws.send(msg);

				current_room_id = current_room_id + 1;
			}

			function onclick_launch_navigation()
			{
				var msg ='{"op":"publish","topic":"/launch_navigation"}';
				console.log(msg);
				ws.send(msg);
			}

			function onclick_start_patrol()
			{
				// prepare the id to be sent
				var id_array = new Array();
				var id_checked_counter = 0;
				for(var i = 0; i < 48; i++){
					var current_checkbox_id = "checkbox_patients_" + (i+1);
					if(document.getElementById(current_checkbox_id).checked){
						id_array[id_checked_counter] = patients_info[i].id;
						id_checked_counter = id_checked_counter + 1;
					}
				}
				var msg_dic = {"op":"publish", "topic":"/patrol_task", "index":id_array};
				var msg_json = JSON.stringify(msg_dic);
				// console.log(msg_json);
				ws.send(msg_json);
			}

			function onclick_isEnablePatrol()
			{
				var msg ='{"op":"publish","topic":"/isEnablePatrol"}';
				console.log(msg);
				ws.send(msg);
			}

			function onclick_close_amcl()
			{
				var msg ='{"op":"publish","topic":"/close_amcl"}';
				console.log(msg);
				ws.send(msg);
			}

			$(document).ready(function(){
				var processNum = $(".processNum");
				var processBar = $(".processBar");
				//上传准备信息
				$("#file").change(function(){
					var file = document.getElementById('file');
					var fileName = file.files[0].name;
					var fileSize = file.files[0].size;
					processBar.css("width",0); 
					//验证要上传的文件
					if(fileSize > 2*1024*1024*1024){
						// processNum.html('未选择文件');
						document.getElementById('file').value = '';
						return false;
					}else{
						// processNum.html('等待上传');
					}
				})
			
				//提交验证
				$(".submit").click(function(){
					console.log("点击提交按钮");
					if($("#file").val() == ''){
						alert('请先选择文件！');
					}else{
						upload();
					}
				})
			
				//创建ajax对象，发送上传请求
				function upload(){
					var file = document.getElementById('file').files[0];
					var form = new FormData();
					form.append('file',file);
					$.ajax({
						url: 'upload_file_advanced.php',//上传地址
						async: true,//异步
						type: 'post',//post方式
						data: form,//FormData数据
						processData: false,//不处理数据流 !important
						contentType: false,//不设置http头 !important
						xhr:function(){//获取上传进度            
							myXhr = $.ajaxSettings.xhr();
							if(myXhr.upload){
								myXhr.upload.addEventListener('progress',function(e){//监听progress事件
									var loaded = e.loaded;//已上传
									var total = e.total;//总大小
									var percent = Math.floor(100*loaded/total);//百分比
									// processNum.text(percent+"%");//数显进度
									$(".submit").text("确认提交" + percent+"%");
									processBar.css("width",percent+"%");//图显进度}, false);
								});
							}
							return myXhr;
						},
						success: function(data){
							console.log("上传成功");
							// $(".submit").text('上传成功');
						}
					})
				}
			})

			function onclick_patients_checkbox()
			{
				
			}

			function onclick_checkbox_all_select()
			{
				if(document.getElementById('checkbox_patients_all_select').checked){
					for(var i = 0; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = true;
					}
				}else{
					for(var i = 0; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = false;
					}
				}
			}

			function onclick_checkbox_updown_select()
			{
				if(document.getElementById('checkbox_patients_updown_select').checked){
					for(var i = 0; i < 24; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = true;
					}
					for(var i = 24; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = false;
					}
				}else{
					for(var i = 0; i < 24; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = false;
					}
					for(var i = 24; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = true;
					}
				}
			}

			function onclick_checkbox_down_select()
			{
				if(document.getElementById('checkbox_patients_down_select').checked){
					for(var i = 24; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = true;
					}
				}else{
					for(var i = 24; i < 48; i++){
						var current_checkbox_id = "checkbox_patients_" + (i+1);
						document.getElementById(current_checkbox_id).checked = false;
					}
				}
			}

	  </script>

	</head>
	<body onload="connect();">
		<center>
			<h1>服务机器人服务配置</h1>
		
		<table class="table" >
			<tr>
				<td  class="attribute">
					<b><br/><br/>环境数据准备</b>
				</td>
				<td class="value" colspan="1">
					<button type="button"  onclick="javascript:onclick_rosbag_record_start();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">开始录制<br/>数据包</button>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_add_patrol_point();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">添加<br/>配送点</button>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_rosbag_record_end();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">结束录制<br/>数据包</button>
				</td>
				<!-- <td class="value" colspan="1">
					<button type="button" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">下载<br/>数据包</button>
				</td> -->
				<td class="value" colspan="1">
					<a href="download/patrol_raw_data.tar.gz" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;"><br/>下载<br/>数据包</a>
				</td>
			</tr>
			<tr>
				<td class="attribute">
					<b><br/><br/>上传地图与配置</b>
				</td>
				
				<!-- <form action="upload_file_advanced.php" method="post" enctype="multipart/form-data">
					<td class="value" colspan="2">
						<a href="javascript:;" class="a-upload">
							<input type="file" name="file" id="file">点击上传
						</a>
					</td>
					<td class="value" colspan="2">
						<input type="submit" name="submit" value="确认提交" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;"/>
					</td>
				</form> -->

				<!-- <td class="value" colspan="4"> -->
					<!-- <div class="upload">  -->
						<!-- <div class="uploadBox"> -->
							<!-- <span class="inputCover">选择文件</span> -->
							<form enctype="">
								<!-- <td colspan="2"> -->
									<!-- <input type="file" name="file" id="file" /> -->
								<!-- </td> -->
								<td class="value" colspan="1">
									<a href="javascript:;" class="a-upload">
										<input type="file" name="file" id="file">点击上传
									</a>
								</td>
								<td class="value" colspan="1">
									<button type="button" class="submit">确认提交</button>
								</td>
							</form>
							<td class="value" colspan="2">
								<span class="processBar"></span>
								<!-- <span class="processNum">未选择文件</span> -->
							</td>
						<!-- </div> -->
					<!-- </div> -->
				<!-- </td> -->
			</tr>
			<tr>
				<td class = "attribute">
					<b><br/><br/>运营控制</b>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_launch_navigation();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">启动<br/>导航</button>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_start_patrol();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">开始<br/>配送</button>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_isEnablePatrol();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">结束<br/>配送</button>
				</td>
				<td class="value" colspan="1">
					<button type="button" onclick="javascript:onclick_close_amcl();" style="background-color: #242EDB;color: #FFFFFF;height:100px;width:100px;display:inline-block;font-weight:bold;">结束<br/>导航</button>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					<input type="checkbox" name="未知" id="checkbox_patients_all_select" value="0"  onClick = "onclick_checkbox_all_select()">
					<label id='label_patients_all_select'>全区全选</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_1" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_1'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_2" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_2'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_3" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_3'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_4" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_4'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					<input type="checkbox" name="未知" id="checkbox_patients_updown_select" value="0"  onClick = "onclick_checkbox_updown_select()">
					<label id='label_patients_updown_select'>上下半区全选</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_5" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_5'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_6" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_6'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_7" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_7'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_8" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_8'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_9" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_9'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_10" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_10'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_11" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_11'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_12" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_12'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_13" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_13'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_14" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_14'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_15" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_15'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_16" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_16'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_17" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_17'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_18" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_18'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_19" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_19'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_20" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_20'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_21" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_21'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_22" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_22'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_23" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_23'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_24" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_24'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_25" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_25'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_26" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_26'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_27" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_27'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_28" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_28'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_29" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_29'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_30" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_30'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_31" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_31'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_32" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_32'>未知</label>
				</td>
			</tr>
			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_33" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_33'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_34" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_34'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_35" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_35'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_36" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_36'>未知</label>
				</td>
			</tr>

			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_37" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_37'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_38" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_38'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_39" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_39'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_40" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_40'>未知</label>
				</td>
			</tr>

			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_41" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_41'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_42" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_42'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_43" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_43'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_44" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_44'>未知</label>
				</td>
			</tr>

			<tr>
				<td class = "attribute">
					<b></b>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_45" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_45'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_46" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_46'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_47" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_47'>未知</label>
				</td>
				<td class="value" >
					<input type="checkbox" name="未知" id="checkbox_patients_48" value="0"  onClick = "onclick_patients_checkbox()">
					<label id='label_patients_48'>未知</label>
				</td>
			</tr>
		</table>
	</center>
	</body>
</html>
