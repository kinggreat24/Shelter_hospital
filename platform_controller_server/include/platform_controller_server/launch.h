/*
 * @Author: your name
 * @Date: 2020-02-23 21:30:28
 * @LastEditTime: 2020-02-26 12:34:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /platform_controller_server/include/launch.h
 */
#ifndef LAUNCH_H
#define LAUNCH_H

#include <unistd.h>  
#include <stdio.h> 
#include <sys/types.h>  
#include <signal.h>

#include <fstream>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

using namespace std;

class Launch{
public:
    Launch(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
    ~Launch();
protected:
    void onLaunchGamppingCallback(const std_msgs::Empty::ConstPtr& msg);
    void onCloseGmappingCallback(const std_msgs::Empty::ConstPtr& msg);
    void onSaveMapCallback(const std_msgs::String::ConstPtr& msg);

    void onLaunchAMCLcallback(const std_msgs::String::ConstPtr& msg);
    void onCloseAMCLcallback(const std_msgs::Empty::ConstPtr& msg);

    void onLaunchROSBagRecordCallback(const std_msgs::String::ConstPtr& msg);
    void onCloseROSBagRecordCallback(const std_msgs::Empty::ConstPtr& msg);

    void onUploadTimerCallback(const ros::TimerEvent&e);
    void onDownloadTimerCallback(const ros::TimerEvent& e);

    void onTimerTest(const std_msgs::Empty::ConstPtr& msg);
private:

    ros::NodeHandle nh_,nh_private_;

    ros::Subscriber sub_launchGmapping, sub_closeGmapping, sub_saveMap;
    ros::Subscriber sub_launcAMCL, sub_closeAMCL;
    ros::Subscriber sub_launchROSBagRecord, sub_closeROSBagRecord;

    ros::Publisher pub_tts;

    ros::Timer download_timer_,upload_timer_;
    ros::Subscriber timer_test;


    std::string map_save_path_;
    std::string launch_pid_path_;
    std::string shell_path_;

    std::string rosbag_name_;

    char buffer[512];
};

#endif//LAUNCH_H