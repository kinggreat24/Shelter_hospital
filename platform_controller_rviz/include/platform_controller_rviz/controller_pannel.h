/*
 * @Author: your name
 * @Date: 2020-03-03 11:41:01
 * @LastEditTime: 2020-03-05 22:43:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /platform_controller_rviz/include/platform_controller_rviz/controller_pannel.h
 */
#ifndef CONTROLLER_PANNEL_H
#define CONTROLLER_PANNEL_H

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <ros/console.h>
#include <rviz/panel.h>

#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <platform_controller_msgs/Patients.h>
#include <platform_controller_msgs/PatrolTask.h>
#endif



#define LOAD_PATROL_POINTS     1
#define LOAD_PATIENTS          2
#define LAUNCH_AMCL            3
#define CLOSE_AMCL             4
#define LAUNCH_ROSBAG_RECORD   5
#define CLOSE_ROSBAG_RECORD    6
#define LAUNCH_GMAPPING        7
#define CLOSE_GMAPPING         8
#define BEGIN_PATROL           9
#define PATROL_TASK_SUCCESS    10


class QLabel;
class QSpinBox;
class QButtonGroup;
class QRadioButton;
class QCheckBox;
class QPushButton;
class QLineEdit;

namespace cp
{

  class  ControllerPannel: public rviz::Panel
  {
    Q_OBJECT
    public:
        ControllerPannel( QWidget* parent = 0 );
        virtual void load( const rviz::Config& config );
        virtual void save( rviz::Config config ) const;
    protected:
        void initUI();
        void initTopic();

        void onPatientsCallback(const platform_controller_msgs::Patients::ConstPtr& msg);
        void onOperationResultCallback(const std_msgs::String::ConstPtr& msg);
    protected Q_SLOTS:
        void onRadionBtnGroupClicked(int id);
        void onBeginPatrolBtnClicked();
        void onStopPatrolBtnClicked();
        

        void onRosbagRecordClicked();
        void onStopRosbagRecordClicked();
        void onAddPatrolPointClicked();

        void onLoadPatrolPointsClicked();
        void onLoadPatientsBtnClicked();

        void onBeginNavigationClicked();
        void onStopNavigationClicked();
        
    private:
        //录制数据包
        QLineEdit* rosbag_file_;
        QLabel* rosbag_label_;
        QLineEdit* patrol_room_id_;
        QLabel* patrol_room_label_;
        QPushButton* record_rosbag_btn_;
        QPushButton* add_patrol_point_;
        QPushButton* stop_record_rosbag_btn_;
        

        //加载数据
        QLineEdit* patorl_points_file_;
        QPushButton* load_patorl_points_btn_;

        QLineEdit* patients_file_;
        QPushButton* load_patients_btn_;

        //开启导航
        QLabel* map_label_;
        QLineEdit* map_file_;
        QPushButton* begin_navigation_;
        QPushButton* stop_navigation_;


        QPushButton* begin_btn_, *stop_btn_;
        QLabel* label;
        QButtonGroup *button_group_;
        QRadioButton* full_region_, *top_region_, *btmRegion_, *any_region_;
        std::vector<QCheckBox*> checkBoxLists_;
        
        // The ROS publisher for the command velocity.
        ros::Publisher patrol_pub_,stop_patrol_pub_;
        ros::Publisher load_patients_pub_,load_patrol_points_pub_;
        
        ros::Publisher rosbag_record_pub_, stop_rosbag_record_pub_;
        ros::Publisher add_patrol_point_pub_,save_patrol_points_pub_;

        ros::Publisher begin_navigation_pub_,end_navigation_pub_;

        ros::Subscriber patients_sub_;

        ros::Subscriber operation_result_sub_;

        // The ROS node handle.
        ros::NodeHandle nh_;

        platform_controller_msgs::Patients patients_;   //当前病人信息

        int room_num_;
  };
}//end of namespace cp


#endif//CONTROLLER_PANNEL_H